import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDl2720cww2bzPFg70h1hVjYO_Poty9eQo",
  authDomain: "todolist-85c8b.firebaseapp.com",
  projectId: "todolist-85c8b",
  storageBucket: "todolist-85c8b.appspot.com",
  messagingSenderId: "139124429996",
  appId: "1:139124429996:web:5370027a2fd03ea72a7b09",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { db };
