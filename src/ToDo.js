import React, { useState, useEffect } from "react";
import { db } from "./firebase";
import {
  collection,
  addDoc,
  getDocs,
  deleteDoc,
  doc,
  updateDoc,
} from "firebase/firestore";
import "./App.css";

const ToDo = () => {
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState("");
  const [editing, setEditing] = useState(false);
  const [currentTodo, setCurrentTodo] = useState({});

  useEffect(() => {
    const fetchTodos = async () => {
      const todosCollection = await getDocs(collection(db, "todos"));
      setTodos(
        todosCollection.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
      );
    };
    fetchTodos();
  }, []);

  const handleAddTodo = async () => {
    if (newTodo.trim() === "") return;
    try {
      const docRef = await addDoc(collection(db, "todos"), { text: newTodo });
      setTodos([...todos, { id: docRef.id, text: newTodo }]);
      setNewTodo("");
    } catch (error) {
      console.error("Error adding document: ", error);
    }
  };

  const handleDeleteTodo = async (id) => {
    try {
      await deleteDoc(doc(db, "todos", id));
      setTodos(todos.filter((todo) => todo.id !== id));
    } catch (error) {
      console.error("Error deleting document: ", error);
    }
  };

  const handleEditTodo = (todo) => {
    setEditing(true);
    setCurrentTodo(todo);
  };

  const handleUpdateTodo = async () => {
    if (currentTodo.text.trim() === "") return;
    try {
      const todoDoc = doc(db, "todos", currentTodo.id);
      await updateDoc(todoDoc, { text: currentTodo.text });
      setTodos(
        todos.map((todo) => (todo.id === currentTodo.id ? currentTodo : todo))
      );
      setEditing(false);
      setCurrentTodo({});
    } catch (error) {
      console.error("Error updating document: ", error);
    }
  };

  return (
    <div className="todo-container">
      <h1>Todo List</h1>
      <div className="todo-input">
        <input
          type="text"
          value={editing ? currentTodo.text : newTodo}
          onChange={(e) =>
            editing
              ? setCurrentTodo({ ...currentTodo, text: e.target.value })
              : setNewTodo(e.target.value)
          }
        />
        <button onClick={editing ? handleUpdateTodo : handleAddTodo}>
          {editing ? "Update" : "Add"}
        </button>
      </div>
      <ul>
        {todos.map((todo) => (
          <li key={todo.id}>
            <span className="todo-text">{todo.text}</span>
            <div className="todo-actions">
              <button className="edit" onClick={() => handleEditTodo(todo)}>
                Edit
              </button>
              <button
                className="delete"
                onClick={() => handleDeleteTodo(todo.id)}
              >
                Delete
              </button>
            </div>
          </li>
        ))}
      </ul>

      <div className="intro">
        <span> created by</span>
        <h1 style={{ fontSize: "20px" }}>M.Talha Mehmood </h1>
      </div>
      <div className="intro">
        <span>Email</span>
        <h1 style={{ fontSize: "16px" }}>khalidtalha485@gmail.com</h1>
      </div>

      <div className="intro">
        <span>Contact.No</span>
        <h1 style={{ fontSize: "16px" }}>+923046335474</h1>
      </div>
    </div>
  );
};

export default ToDo;
